import React from "react";
import logo from "./logo.svg";
import { Counter } from "./features/counter/Counter";
import "./App.css";
import { Router } from "./features/Routes/Route";
import { Col, Layout, Row } from "antd";
import { GameProvider } from "./features/Component/Context/GameContext";
const { Content, Header } = Layout;
function App() {
  // React fragment why use this
  return (
    <GameProvider>
      <Layout>
        <Header>
          <div>
            <h3 className="logo">EMUMBA</h3>
          </div>
        </Header>
        <Content>
          <Row>
            <Col span={12}>
              <Router />
            </Col>
            <Col span={12}>
              <Counter />
            </Col>
          </Row>
        </Content>
      </Layout>
    </GameProvider>
  );
}

export default App;
