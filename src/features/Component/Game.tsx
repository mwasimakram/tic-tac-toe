import { Col, Row } from "antd";
import { is } from "immer/dist/internal";
import { useContext, useEffect, useState } from "react";
import { json } from "stream/consumers";
import { Board } from "./Board";
import { GameContext } from "../Component/Context/GameContext";
interface History {
  squares: string[];
}
interface NextState {
  isNext: boolean;
}
interface StepState {
  stepNumber: number;
}
export function Game() {
  const {
    _history,
    isNext,
    stepNumber,
    updateNextState,
    updateHistoryState,
    updateStepNumberState,
  } = useContext(GameContext);
  let history = _history;

  const current = history[stepNumber];
  const winner = calculateWinner(current.squares);
  let status;
  if (winner) {
    status = `Winner is ${winner}`;
  } else {
    status = `Next player is ${isNext ? "X" : "O"}`;
  }
  const moves = history.map((step: {}, move: number) => {
    const desc = move ? "Go to move #" + move : "Go to game start";
    console.log(step);
    return (
      <li>
        <button onClick={() => jumpTo(move)}>{desc}</button>
      </li>
    );
  });
  function jumpTo(step: any) {
    updateStepNumberState?.(step);
    updateNextState?.(step % 2 === 0);
  }
  function handleClick(i: number) {
    const history = _history.slice(0, stepNumber + 1);
    const current = history[history.length - 1];
    const squares = current.squares.slice();
    if (calculateWinner(squares) || squares[i]) {
      return;
    }
    squares[i] = isNext ? "X" : "O";
    let newHistory = history.concat([
      {
        squares: squares,
      },
    ]);
    updateHistoryState?.(newHistory);
    updateNextState?.(!isNext);
    updateStepNumberState?.(history.length);
  }

  return (
    <>
      <Row>
        <Col span={4}>
          <h4 className="status">{status}</h4>
        </Col>
        <Col span={12}>
          <div className="board">
            <Board
              squares={current.squares}
              onClick={(i: number) => handleClick(i)}
            />
          </div>
        </Col>

        <Col span={8}>
          <div>{moves}</div>
        </Col>
      </Row>
    </>
  );
}
function calculateWinner(squares: string[]) {
  const lines = [
    [0, 1, 2],
    [3, 4, 5],
    [6, 7, 8],
    [0, 3, 6],
    [1, 4, 7],
    [2, 5, 8],
    [0, 4, 8],
    [2, 4, 6],
  ];
  for (let i = 0; i < lines.length; i++) {
    const [a, b, c] = lines[i];
    if (squares[a] && squares[a] === squares[b] && squares[a] === squares[c]) {
      return squares[a];
    }
  }
  return null;
}
