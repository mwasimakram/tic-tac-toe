import { is, set } from "immer/dist/internal"; // 2 why use this 3 context api
import { useState } from "react";
import "./index.css";
import { Square } from "./Square";
interface Iprop{ //types of all props etc 
    squares:(string)[],
    onClick:(i:number)=>void
};
export function Board(props:Iprop) {
  function renderSquare(i: number) {
    return (
      <Square
        value={props.squares[i]}
        onClick={() => {
          props.onClick(i);
        }}
      />
    );
  }
  
  return (
    <>
      <div className="board-row">
        {renderSquare(0)}
        {renderSquare(1)}
        {renderSquare(2)}
      </div>
      <div className="board-row">
        {renderSquare(3)}
        {renderSquare(4)}
        {renderSquare(5)}
      </div>
      <div className="board-row">
        {renderSquare(6)}
        {renderSquare(7)}
        {renderSquare(8)}
      </div>
    </>
  );
}


// 1/ responsive
// 2/ center align board
// 3/ make two sections (done)
// 1 for tic tac toe
// 2 redux counter
// 4/ add context api for tic tac toe
// 5/ add appropriate types (done)

// 6/ add localstorage, to preserve game state