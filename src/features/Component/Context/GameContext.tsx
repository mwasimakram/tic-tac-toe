import { createContext, FC, useEffect, useState } from "react";

interface Props {
  children: React.ReactNode;
}
interface History {
  squares: string[];
}
interface NextState {
  isNext: boolean;
}
interface StepState {
  stepNumber: number;
}
interface IGameContext {
  _history: any;
  isNext: boolean;
  stepNumber: number;
  updateNextState?:(isNext:boolean)=>void,
  updateHistoryState?:(history:(History)[])=>void,
  updateStepNumberState?:(step:number)=>void
}
const defaultState = {
    _history:[],
    isNext: true,
    stepNumber: 0,
};
export const GameContext = createContext<IGameContext>(defaultState);
export const GameProvider: FC<Props> = ({ children }) => {
  const [_history, sethistory] = useState<History[]>([
    { squares: Array(9).fill(null) },
  ]);
  const [isNext, setIsNext] = useState(defaultState.isNext);
  const [stepNumber, setStepNumber] = useState(defaultState.stepNumber);
  const updateNextState = (isNext:boolean) => {
      setIsNext(isNext);
      localStorage.setItem('nextIs',JSON.stringify(isNext));

  };
  const updateHistoryState =(history:(History)[])=>{
    sethistory(history);
    localStorage.setItem('history',JSON.stringify(history));

  }
  const updateStepNumberState = (step:number)=>{
      setStepNumber(step);
      localStorage.setItem('stepNumber',JSON.stringify(stepNumber));

  }
//   useEffect(()=>{
//     localStorage.setItem('stepNumber',JSON.stringify(stepNumber));
//   },[stepNumber]);
  useEffect(()=>{
    const stepsNumber = localStorage.getItem('stepNumber');
    setStepNumber(Number(stepsNumber));
    const nextIs = localStorage.getItem('nextIs');
    setIsNext(Boolean(nextIs));
    const storedHistory = localStorage.getItem('history');
    if(storedHistory) sethistory(JSON.parse(storedHistory));
  },[])
  return (
    <GameContext.Provider value={{ _history, isNext, stepNumber,updateNextState,updateHistoryState,updateStepNumberState}}>
      {children}
    </GameContext.Provider>
  );
};
