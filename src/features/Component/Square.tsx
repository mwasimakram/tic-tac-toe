import { useState } from "react"
import "./index.css"
interface Iprops{
    value:string,
    onClick:()=>void
}
export function Square(props:Iprops){
    return(
        <>
            <button className="square" onClick={()=>{props.onClick()}}>{props.value}</button>
        </>
    )
}